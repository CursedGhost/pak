#pragma once

#include "pak.h"

#ifdef __cplusplus
extern "C" {
#else
#define bool unsigned char
#endif

//----------------------------------------------------------------
// Command arguments

// Arguments wrapper, i is the current index of arguments being parsed
struct PAKtool_args
{
	int i;
	int c;
	char** v;

	const char* file;
	struct PAK_key key;
	const struct PAKtool_cmd* cmd;
};

// Parse a hexadecimal key string
bool PAKtool_parseKey(const char* str, struct PAK_key* key);
// Parse the key argument
bool PAKtool_parseKeyArg(struct PAKtool_args* args);

// Sub commands
struct PAKtool_cmd
{
	const char* str;
	int(*pfn)(struct PAKtool_args* args, struct PAK* pak);
};

// Parse the invoked subcommand
const struct PAKtool_cmd* PAKtool_parseCmd(const char* cmd);

// Parse the file, key and cmd args
int PAKtool_parse(struct PAKtool_args* args);

// Invoke the args command
int PAKtool_invoke(struct PAKtool_args* args);

//----------------------------------------------------------------
// Command handlers

// Create a new empty PAK file
int PAKtool_touch(struct PAKtool_args* args);
// Check the PAK file for errors
int PAKtool_fsck(struct PAKtool_args* args, struct PAK* pak);
// List all files in the PAK file
int PAKtool_ls(struct PAKtool_args* args, struct PAK* pak);
// Output files in the PAK file
int PAKtool_cat(struct PAKtool_args* args, struct PAK* pak);
// Add files to the PAK file
int PAKtool_add(struct PAKtool_args* args, struct PAK* pak);
// Remove files from the PAK file
int PAKtool_rm(struct PAKtool_args* args, struct PAK* pak);
// Move files inside the PAK file
int PAKtool_mv(struct PAKtool_args* args, struct PAK* pak);
// Unpack files from the PAK file
int PAKtool_unpack(struct PAKtool_args* args, struct PAK* pak);
// Garbage collect the PAK file
int PAKtool_gc(struct PAKtool_args* args, struct PAK* pak);

#ifdef __cplusplus
}
#else
#undef bool
#endif
