Encrypted PAK File Format
==========================



Compiling
---------------------

Using the provided solution (project/pak.sln), Rebuild will fail on paktool (as they use the same output directory, which gets cleaned before paktool is build). Clean and then Build to compile. Output can be found in bin\, 64 suffix means x64, d suffix means Debug.



License
---------------------

Licensed under MIT license, see license.txt.

What does this mean? You can do anything you want (eg using in a closed source for profit project) as long as you provide a copy of this license somewhere.



PAK Tool
---------------------

Command line tool to manipulate PAK files; KEY argument is either -kHEX or --key=HEX where it expects 16 hexadecimal digits (128 bit key). Without the HEX the key is assumed to be all zeroes without printing a warning. KEY argument can be omitted where key will be assumed all zeroes but will print a warning.

    paktool FILEPAK [KEY] touch

Creates a new PAK file, it writes just the header. Encryption is not optional, when KEY is omitted it'll simply set the key to all zeroes and print a warning message. Use this for testing only. NOTE! Overwrites any existing files!

    paktool FILEPAK [KEY] fsck

Validates the descriptors.

    paktool FILEPAK [KEY] ls [MATCH]

Lists all the files in the PAK file. Optionally starting from a certain subdirectory.

    paktool FILEPAK [KEY] cat FILE1 [FILE2...]

Prints the request files contents to stdout.

    paktool FILEPAK [KEY] add [DEST1:]SRC1 [[DEST2:]SRC2...]

Adds a number of files to the PAK file. DEST is the filename given to the file inside the PAK, if omitted will use SRC. SRC is the file on disk (passed to fopen). May add any number of files. Error if DEST already exists! Directory descriptors are automatically added as needed.

    paktool FILEPAK [KEY] rm FILE1 [FILE2...]

Deletes a descriptor (either file or directory). Does not remove the file contents, it just deletes the descriptor! Use `gc` to repack the whole file. If used on a directory, all children are moved to the parent directory.

    paktool FILEPAK [KEY] mv SRC1:DEST1 [SRC2:DEST2...]

Renames files inside the PAK.

    paktool FILEPAK [KEY] unpack SRC1[:DEST1] [SRC2[:DEST2]...]

Unpacks files from the PAK. If DEST is omitted will use SRC.

    paktool FILEPAK [KEY] gc

Since removing files does not remove the file contents, this leaves garbage around and increases file size. This command will garbage collect the whole PAK file and reclaim these unused blocks.



Programming Usage
---------------------

First run install.bat, which will register the env var %LIBPAK_PATH% and set it to the directory install.bat is in. This allows you to reference this directory inside Visual Studio using $(LIBPAK_PATH). Visual Studio must be restarted before it'll recognize the new env var.

Make sure you've compiled for every configuration and platform you wish to use (Debug/Release/x64). Visual Studio 2013 was used to build, any other should work but you may need to adjust the Platform Toolset under the Configuration Properties of libpak and paktool to whatever you support. The Platform Toolset must match with the project you're trying to integrate with!

Add `$(LIBPAK_PATH)\public` to your C/C++ Additional Include Directories and `$(LIBPAK_PATH)\bin` to your Linker's Additional Library Directories.

Add `libpak[64][d].lib` to your Additional Dependencies. 64 suffix for x64 targets, d suffix for Debug configurations.

`#include "pak/pak.h"` to your project's source to start using the library.

Editing and creating PAK files require providing a source of random numbers to generate IVs, a default implementation is provided for windows `PAK_rand` which uses [rand_s](http://msdn.microsoft.com/en-us/library/sxtz2fa8.aspx) for cryptographic secure random numbers.

There are three ways to access PAK files, take note of the access specifiers in fopen (error checking omitted for brevity):

* Use `PAK_create` to create a new PAK file:

        PAK_key key = /* Initialize the key */;
        FILE* file = fopen(filename, "w+b");
        PAK* pak = PAK_create(&key, &PAK_rand, file);
        // Do work with pak
        PAK_close(pak);
        fclose(file);

* Use `PAK_file` to open an existing PAK file:

        PAK_key key = /* Initialize the key */;
        bool canedit = /* Editing requires write access, otherwise read-only is fine */;
        FILE* file = fopen(filename, canedit ? "r+b" : "rb");
        PAK* pak = PAK_file(&key, file);
        // Do work with pak
        PAK_close(pak);
        fclose(file);

* Use `PAK_memory` to open a read-only PAK file in memory:

        PAK_key key = /* Initialize the key */;
        const void* begin = /* Beginning of the PAK file in memory */;
        const void* end = /* End of the PAK file in memory */;
        PAK* pak = PAK_memory(&key, begin, end);
        // Do work with pak
        PAK_close(pak);

Read the contents of a specific file:

    PAK* pak = /* PAK file opened earlier */;
    PAK_desc desc;
    if ( PAK_find(pak, &desc, "foo/bar/hello.txt") ) {
        char* data = (char*)malloc(desc.ContentSize + 1);
        PAK_address addr = { 0, desc.ContentSize };
        PAK_udec(pak, &desc.Section, &addr, data);
        data[desc.ContentSize] = '\0';
        // File contents read in data var
        printf("%s\n", data);
    }
    // File not found
    PAK_close(pak);

Iterating over files is for another time, check out PAKtool_ls in public\pak\paktool.cpp if you want to know how!

Modifying existing PAK files (doesn't work with memory PAKs); note that you cannot use reading functions like `PAK_find` while editing is in progress. Calling these read functions is harmless and will make them fail (return false)! Only one edit can be in progress for a given pak. Forgetting to call `PAK_end` will corrupt the pak file (as the descriptors were overwritten by newly added files and the new descriptors haven't be written yet)!

    PAK* pak = /* PAK file opened earlier */;
    PAK_unit reserve = /* Max number of extra descriptors you can add later */;
    PAK_edit* edit = PAK_begin(pak, reserve, &PAK_rand);
    // Now use the edit token to make modifications, example to add a new file:
    char contents[16] = "Hello New World!";
    PAK_section sec; /* Section allocated for your file */
    PAK_unit type = /* A non-zero content type, entirely user defined (eg 1), ends up in PAK_desc::ContentType */;
    PAK_unit size = sizeof(contents); /* Actual size of the content you want to write, ends up in PAK_desc::ContentSize */
    PAK_unit reserve = size; /* Space reserved for the section containing the contents, will be aligned to block size */
    if ( PAK_add(edit, type, size, size, &sec, "foo/bar/hello.txt") ) {
        // Write the contents of the new file
        PAK_address addr = { 0, sec.Addr.Size }; /* For now, only aligned encrypt (PAK_aenc) is available, requires aligned address */
        PAK_aenc(pak, &sec, &addr, contents);
    }
    PAK_end(edit);
    PAK_close(pak);


File Format
---------------------

Three major sections in the format:

1. Header

   The header starts with 16 bytes of IV followed by another 16 bytes: an unused `Version` identifier, the `TotalSize` of the PAK, and the address of the `Root` directory.

2. Files Contents

   All files are stuffed here in no particular order, each file is 16 byte aligned and prefixed with a 16 byte IV (not included in the Address). Gaps may be accumulated by manipulating the PAK file, paktool comes with a `gc` command to clean this up.

3. File and Directory Descriptors

   This is a series of `PAK_desc` entries, the size should always be a multiple of `sizeof(PAK_desc)`.

   If `ContentType` is zero then this entry is a subdirectory and the following `Section.Range.Size` bytes are all part of it, `Section.Range.Offset` and `ContentSize` must be zero. Multiple subdirectory siblings with the same name are not allowed, they must be merged together!

   Nonzero `ContentType`s are files (meaning is entirely user defined). Each file is contained in a section and is `ContentSize` bytes long. Encryption requires IVs to avoid key reuse; in this implementation each file gets its own IV, specified in `Section.IV`.

   Example flat list of file descriptors:

        "test.txt"   (test.txt)
        "subdir"     (subdir/     Size: 6*sizeof(PAK_desc))
        "file.txt"   (subdir/file.txt)
        "stuff.dat"  (subdir/stuff.dat)
        "more"       (subdir/more/  Size: 3*sizeof(PAK_desc))
        "foo.txt"    (subdir/more/foo.txt)
        "baz.txt"    (subdir/more/baz.txt)
        "bar.txt"    (subdir/more/bar.txt)
        "bin"        (bin/        Size: 2*sizeof(PAK_desc))
        "core.dll"   (bin/core.dll)
        "empty"      (bin/empty/  Size: 0*sizeof(PAK_desc))

   The descriptors at the end of the file makes it efficient to add new files. This is a requirement!
